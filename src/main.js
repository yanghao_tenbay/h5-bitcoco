// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import '@/lib/flexible'
import '../src/css/index.scss'
import  MuseUI from  'muse-ui'
import '@/css/muse-ui.css'
import axios from 'axios';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import touch from 'vue-directive-touch';

import('@/assets/js/layer.js')
import('@/assets/css/layer.css')

Vue.config.productionTip = false
Vue.use( MuseUI)
Vue.use(ElementUI);
Vue.use(touch);

axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
Vue.prototype.$http = axios;
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
